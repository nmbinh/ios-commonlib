//
//  AppDelegate.h
//  CommonLibTest
//
//  Created by Binh Nguyen on 6/16/15.
//  Copyright (c) 2015 Binh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

