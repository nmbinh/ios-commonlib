//
//  BaseContainerViewController.m
//  mSafra
//
//  Created by Binh Nguyen on 6/1/15.
//  Copyright (c) 2015 Apps-Cyclone. All rights reserved.
//

#import "BaseViewContainerVC.h"

@interface EmbedSubViewToContainerSegue: UIStoryboardSegue
@end

@implementation EmbedSubViewToContainerSegue
-(void) perform {

    BaseViewContainerVC *containerVC = (BaseViewContainerVC *)self.sourceViewController;
    UIViewController *destinationViewController = (UIViewController *) containerVC.destVC;
    
    //remove old viewController
    if (containerVC.sourceVC) {
        [containerVC.sourceVC willMoveToParentViewController:nil];
        [containerVC.sourceVC.view removeFromSuperview];
        [containerVC.sourceVC removeFromParentViewController];
    }
    
    destinationViewController.view.frame = containerVC.ViewContainer.bounds;
    
    [containerVC addChildViewController:destinationViewController];
    [containerVC.ViewContainer addSubview:destinationViewController.view];
    [destinationViewController didMoveToParentViewController:containerVC];
}
@end




@implementation BaseViewContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(self.viewContainer_DefaultTab){
        [self.viewContainer_DefaultTab sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if(![segue isKindOfClass: [EmbedSubViewToContainerSegue class]]){
        [super prepareForSegue:segue sender:sender];
        return;
    }
    
    self.sourceVC = self.destVC;
    
    //if the subview is displaying at foreground, don't add it again.
    if(self.sourceVC == segue.destinationViewController){
        return;
    }

    //Update status of corresponding button.
    [self.viewContainer_TabControls setValue:@NO forKeyPath:@"selected"];
    [sender setSelected:YES];
    
    self.selectedIndex = [self.viewContainer_TabControls indexOfObject:sender];
    self.destVC = segue.destinationViewController;
    
}
@end
