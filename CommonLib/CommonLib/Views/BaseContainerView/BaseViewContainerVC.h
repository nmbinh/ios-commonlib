//
//  BaseContainerViewController.h
//  mSafra
//
//  Created by ; Nguyen on 6/1/15.
//  Copyright (c) 2015 Apps-Cyclone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewContainerVC : UIViewController
@property (weak,nonatomic) UIViewController *destVC;
@property (strong, nonatomic) UIViewController *sourceVC;
@property (weak,nonatomic) IBOutlet UIView *ViewContainer;

@property (assign, nonatomic) NSInteger selectedIndex;

@property (nonatomic,strong) IBOutletCollection(UIButton) NSArray *viewContainer_TabControls;
@property (nonatomic) IBOutlet UIButton *viewContainer_DefaultTab;

-(void)viewDidLoad __attribute__((objc_requires_super));
- (void)viewWillAppear:(BOOL)animated __attribute__((objc_requires_super));
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender __attribute__((objc_requires_super));

@end
