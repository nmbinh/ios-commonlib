//
//  BaseViewController.h
//  mSafra
//
//  Created by Binh Nguyen on 5/15/15.
//  Copyright (c) 2015 Apps-Cyclone. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UITextFieldHelperVC : UIViewController <UITextFieldDelegate>

    @property UIView* vwMovableView;
    @property int moveViewAmount;//This is amount of pixel we should move the view up.
    @property int keyboardHeight; //Hard code the height of keyboard. When it display, we will get the real height.
    @property NSMutableArray* arrTextFields;
    @property int currentTextFieldIndex;
    @property int originalYAxis;
    @property (strong, nonatomic) IBOutletCollection(UIView) NSArray *vwWhiteRoundedCornerViews;
    @property UIReturnKeyType returnKeyTypeWhenDone;


    -(void)viewDidLoad __attribute__((objc_requires_super));
    -(void)viewWillAppear:(BOOL)animated __attribute__((objc_requires_super));
    - (void)viewDidAppear:(BOOL)animated __attribute__((objc_requires_super));
    -(void)viewWillDisappear:(BOOL)animated __attribute__((objc_requires_super));
    - (BOOL)textFieldShouldReturn:(UITextField *)textField __attribute__((objc_requires_super));

@end