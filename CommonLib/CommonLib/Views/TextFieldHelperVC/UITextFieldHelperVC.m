//
//  BaseViewController.m
//  mSafra
//
//  Created by Binh Nguyen on 5/15/15.
//  Copyright (c) 2015 Apps-Cyclone. All rights reserved.
//


#import "UITextFieldHelperVC.h"

/*
 This is a general view controller. It listens to keyboard's displaying events and help to
 push up the main view if the keyboard cover the focusing text field. Whenever user touchs out side
 of text fields the keyboard will dismiss, then the main view will be pulled down to original position.
 */
@interface UITextFieldHelperVC ()

@end

@implementation UITextFieldHelperVC

- (void)viewWillAppear:(BOOL)animated{
    //Look up for child views which want to be rounded corners and make it up.
    for (UIView* view in self.vwWhiteRoundedCornerViews) {
        [self setRoundedCornerBackgroundToView:view];
    }
    self.returnKeyTypeWhenDone = UIReturnKeyDone;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.moveViewAmount = 0;
    self.keyboardHeight = 216;
    self.arrTextFields = nil;
    self.currentTextFieldIndex = -1;
    self.originalYAxis = 0;
    
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    //Register notification when a textfield will be editted.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidBeginEditting:)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:nil];

    
    //Use TapGestureRecognizer to listen to event that user touches outside of textfields.
    self.view.userInteractionEnabled = YES;
    UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(touchOutsideToDismiss:)];
    //Set this property to NO, so that this gesture will not hide touch event on other control such as UITableViewCell, Label,...
    tabGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tabGesture];
    
    [self findAllTextFieldsInView:self.view];
    
    if(_vwMovableView == nil)
        _vwMovableView = self.view;
    _originalYAxis = _vwMovableView.frame.origin.y;

    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds;
    
    //Get the real height of keyboard
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&keyboardBounds];
    _keyboardHeight = keyboardBounds.size.height;
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    //Move view back when the keyboard dismiss.
    [self moveContainerViewTo:self.originalYAxis];

}

- (void) textFieldDidBeginEditting: (NSNotification*)notification{
    
    //Get current focused view, if the keyboard covers it, move the view up to show it.
    UITextField* currentView = [self getFocusingTextField:self.view];
    
    if(currentView == nil){
        return;
    }
    
    currentView.returnKeyType = _currentTextFieldIndex == _arrTextFields.count - 1? self.returnKeyTypeWhenDone : UIReturnKeyNext;
    
    //Convert the local coordinates to global coordinates (I mean the position of it within application windows)
    CGRect currentViewRect =  [currentView.superview convertRect:currentView.frame toView:self.view];
    
    int viewBottom = currentViewRect.origin.y + currentViewRect.size.height;
    int screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    if(viewBottom >  screenHeight - _keyboardHeight){
        //moveViewAmount = keyboardHeight - viewBottom;
        _moveViewAmount = _vwMovableView.frame.origin.y - (viewBottom - (screenHeight - _keyboardHeight) + self.originalYAxis) - 10;
        [self moveContainerViewTo: _moveViewAmount];
    }

}


- (UITextField*) getFocusingTextField:(UIView *)view{
    //The below code block works in the case you don't want to handle keyboard return key.
//    if(view.isFirstResponder)
//        return view;
//
//    for (UIView* sub in view.subviews) {
//        UIView* found = [self getFocusingTextField:sub];
//        if(found != nil)
//            return found;
//    }
//    return nil;
    
    /**This code block will find the list of textfields, save current focusing textfield's index since we
     * need these informations to handle keyboard return key. If user press button 'Next', the next textfield
     * will got forcus. If user presses button 'Done', it will call default button's action.
     */
    if(self.arrTextFields == nil){
            self.arrTextFields = [self findAllTextFieldsInView:self.view];
        self.arrTextFields = [self sortSubviewByPosition:self.arrTextFields];
    }
    for (int i = 0; i< self.arrTextFields.count; i ++) {
        if(((UITextField*) self.arrTextFields[i]).isFirstResponder){
            self.currentTextFieldIndex = i;
            return self.arrTextFields[i];
        }
    }
    return nil;
}


-(NSMutableArray*)findAllTextFieldsInView:(UIView*)view{
    NSMutableArray* arr = [[NSMutableArray alloc] init];
    for(id x in [view subviews]){
        if([x isKindOfClass:[UITextField class]]){
            [arr addObject:x];
            ((UITextField*)x).delegate = self;
        }else if([x isKindOfClass:[UISearchBar class]]){ //UISearchBar contains UITextField.
            UITextField *searchTextField = [self findTextFieldInsideSearchBar:x];
            if(searchTextField != nil){
                [arr addObject:searchTextField];
//                [searchTextField setText:@"sfsfsdf"];
//                [searchTextField setDelegate:self];
            }
        }else
            if([x respondsToSelector:@selector(subviews)]){
            // if it has subviews, loop through those, too
            [arr addObjectsFromArray:[self findAllTextFieldsInView:x]];
        }
    }
    return arr;
}
- (UITextField*) findTextFieldInsideSearchBar:(UISearchBar*) searchBar{

    for (UIView* view in searchBar.subviews) {
        if([[UIDevice currentDevice] systemVersion].floatValue >= 7){
            //at iOS v7.0 and later,
            for (UIView* subSubView in view.subviews) {
                if([subSubView isKindOfClass:[UITextField class]])

                    return (UITextField*) subSubView;
            }
        }else{
            //iOS v6.0 and erlier.
            if([view isKindOfClass:[UITextField class]])
                return (UITextField*) view;
        }
    }
    return nil;
}
- (NSMutableArray*) sortSubviewByPosition:(NSMutableArray*) arr  {
    for(int i = 0; i < arr.count-1; i ++)
        for(int j = i + 1; j < arr.count; j ++ ){
            UIView* view1 = [arr objectAtIndex:i];
            UIView* view2 = [arr objectAtIndex:j];
            
            CGRect rectView1, rectView2;
            rectView1 = [view1.superview convertRect:view1.frame toView:self.view];
            rectView2 = [view2.superview convertRect:view2.frame toView:self.view];
            
            if(rectView2.origin.y < rectView1.origin.y ||
               ((rectView2.origin.y == rectView1.origin.y )&&(rectView2.origin.x < rectView1.origin.x))){
                UIView* temp = view1;
                [arr replaceObjectAtIndex:i withObject:view2];
                [arr replaceObjectAtIndex:j withObject:temp];
            }
        }
    
    return arr;
}

- (void) moveContainerViewTo:(int) yAxis{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
    self.vwMovableView.frame = CGRectMake(0,yAxis,self.vwMovableView.frame.size.width,self.vwMovableView.frame.size.height);
    [UIView commitAnimations];

}
- (void) touchOutsideToDismiss:(id)sender {
    [self.view endEditing:YES];
}
- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if(self.currentTextFieldIndex < self.arrTextFields.count - 1){
        [((UITextField*)[self.arrTextFields objectAtIndex:self.currentTextFieldIndex]) resignFirstResponder];
        [((UITextField*)[self.arrTextFields objectAtIndex:self.currentTextFieldIndex + 1]) becomeFirstResponder];
    }else{
        [[self.arrTextFields objectAtIndex:self.currentTextFieldIndex] resignFirstResponder];
        [self onKeyboardDoneAction];

    }
    return YES;
}


//Make up a view with a white rounded corner background.
- (void) setRoundedCornerBackgroundToView:(UIView*)view{
    [view.layer setCornerRadius:5.0f];
    [view.layer setBorderColor:[UIColor whiteColor].CGColor];
    [view.layer setBorderWidth:1.5f];
    
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(0.0, 2.0)];

}
- (void)onKeyboardDoneAction{
    
}


@end
