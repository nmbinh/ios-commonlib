//
//  BaseViewController.h
//
//
//  This is a basic view controller, It handles basic setings which most view controllers should do.
//For example: background task executor, RESTful connector, ...


//  <<These settings will be added time by time.>>
//
//  All other view controller should extend this VC from now on.

//  Created by Binh Nguyen on 6/18/15.
//  Copyright (c) 2015 Binh Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@end
