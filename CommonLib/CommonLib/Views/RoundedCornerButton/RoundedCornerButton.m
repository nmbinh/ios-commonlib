//
//  RedButton.m
//  mSafra
//
//  Created by Binh Nguyen on 5/29/15.
//  Copyright (c) 2015 Apps-Cyclone. All rights reserved.
//

#import "RoundedCornerButton.h"

@implementation RoundedCornerButton
-(void)drawRect:(CGRect)rect{
    
    self.layer.borderWidth = self.borderWidth;
    self.layer.borderColor = self.borderColor.CGColor;
    
    [self.layer setCornerRadius:self.cornerRadius];
    self.clipsToBounds = YES;
    
}
@end
