//
//  RedButton.h
//  mSafra
//
//  Created by Binh Nguyen on 5/29/15.
//  Copyright (c) 2015 Apps-Cyclone. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface RoundedCornerButton : UIButton
@property (nonatomic) IBInspectable int cornerRadius;
@property (nonatomic) IBInspectable int borderWidth;
@property (nonatomic) IBInspectable UIColor* borderColor;
@end
